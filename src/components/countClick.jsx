import { Component } from "react";

class CountClick extends Component {
    constructor (props){
        super(props)
        this.state = {
            count : 0
        }
    }

    onBtnDownClick = () => {
        this.setState({
            count: this.state.count - 1
        })
    }
    onBtnUpClick = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    render (){
        return (
            <>
            <button onClick={this.onBtnUpClick} className="btn btn-success">Tăng</button>
            <button onClick={this.onBtnDownClick} className="btn btn-danger">Giảm</button>
            <p>Count {this.state.count}</p>
            </>
        )
    }
}

export default CountClick